#!/bin/bash

dotfiles_arg1 = "$1"

test $USER = root && dotfiles_userdir="/root" || dotfiles_userdir="/home/$USER"

# Get distro name and what distro it's based on
test -f /etc/os-release && . /etc/os-release && dotfiles_os="$ID" && dotfiles_os_like="$ID_LIKE"

# set package manager command for installation on arch based distros
test "$dotfiles_os_like" = "arch" || test "$dotfiles_os" = "manjaro" && dotfiles_install="trizen --noedit --needed --noinfo -S"

# set package manager command for installation on ubuntu based distros
test "$dotfiles_os_like" = "ubuntu" || test "$dotfiles_os" = "ubuntu" || test "$dotfiles_os" = "mint" && dotfiles_install="sudo apt install"

# set package manager command for installation on fedora
test "$dotfiles_os" = "fedora" && dotfiles_install="sudo dnf install"

# Check if trizen is installed. If not: install it. Only applies to arch based distros.
test "$dotfiles_os_like" = "arch" || test "$dotfiles_os" = "manjaro" && \
which trizen > /dev/null || git clone https://aur.archlinux.org/trizen.git $dotfiles_userdir/ && cd trizen && makepkg -si && cd .. && rm trizen

# Check if git is installed. If not: Install it.
which git > /dev/null || $dotfiles_install git

# Clone dotfiles to ~/Dotfiles
test "$dotfiles_arg1" = "--no-clone" || git clone https://github.com/Swedneck/Dotfiles.git "$dotfiles_userdir"/Dotfiles

# Check if zsh is installed. If not: Install it.
which zsh > /dev/null || $dotfiles_install zsh

# Check if oh-my-zsh is installed. If not: Install it.
test -d $dotfiles_userdir/.oh-my-zsh || sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

# Check if programs are installed and install them if they're not.
which neofetch > /dev/null || $dotfiles_install neofetch
which vim > /dev/null || $dotfiles_install vim
which thefuck > /dev/null || $dotfiles_install thefuck
which zsh-syntax-highlighting > /dev/null || $dotfiles_install zsh-syntax-highlighting

neofetch
