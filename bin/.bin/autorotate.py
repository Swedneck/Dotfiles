#!/usr/bin/python

#This script is based on rotate.py in https://github.com/borlum/tpRotate
#Edited to work on Thinkpad x220t (possibly other models) with additional functionality by Tim Stahel https://gitlab.com/Swedneck/thinkpad-autorotation

import os,time,re,sys;

stylus = 9
eraser = 14
touch = 10

#Rotating the display
normal = 'xrandr --output LVDS1 --rotate normal; xsetwacom set %s rotate none; xsetwacom set %s rotate none; xsetwacom set %s rotate none' % (stylus, eraser, touch)
inverted = 'xrandr --output LVDS1 --rotate inverted; xsetwacom set %s rotate half; xsetwacom set %s rotate half; xsetwacom set %s rotate half' % (stylus, eraser, touch)
left = 'xrandr --output LVDS1 --rotate left; xsetwacom set %s rotate ccw; xsetwacom set %s rotate ccw; xsetwacom set %s rotate ccw' % (stylus, eraser, touch)
right = 'xrandr --output LVDS1 --rotate right; xsetwacom set %s rotate cw; xsetwacom set %s rotate cw; xsetwacom set %s rotate cw' % (stylus, eraser, touch)

#Reading data from accelerometer (x-axis)
def readAccelerometer():
    f = open('/sys/devices/platform/hdaps/position','r')
    return int(re.search('[-]*[0-9]+', f.readline()).group(0))

def tablet_mode():
    f = open('/sys/devices/platform/thinkpad_acpi/hotkey_tablet_mode','r')
    return int(f.read())

while True:
    time.sleep(0.1)
    data = abs(readAccelerometer())

    if (tablet_mode() == 1):
        if (data < 385):
            os.system(left)
        elif (data > 635):
            os.system(right)
        elif ((data < 515) and (data > 495)):
            os.system(inverted)
    else:
        if (data < 385):
            os.system(right)
        elif (data > 635):
            os.system(left)
        elif ((data < 515) and (data > 495)):
            os.system(normal)
