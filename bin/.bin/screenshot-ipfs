#!/bin/bash

# Takes a screenshot with scrot and adds it to ipfs, then puts the hash of the
# screenshot into the clipboard.

# Requires a running ipfs node to work, https://ipfs.io/ipns/ipfs.io/docs/install/


# store use message to $use variable
read -r -d '' use <<- EOM
screenshot-ipfs, GPL v3 by Tim Stahel
This script accepts options in both "--option value" and "--option=value" formats
Config file: ~/.config/screenshot-ipfs.conf
Options:
 -g --ipfs-gateway        Which IPFS gateway to use
 -d --screenshot-dir      Directory to put screenshots in
 -c --use-ipfs-cluster    Pin to cluster (yes/no)
 -s --screenshot-software Screenshot software to use
 -m --mfs-dir             Organize screenshots with mfs
 -h --usage               Show this usage text

Some example ipfs gateways are ipfs.io and siderus.io
Info and docs for ipfs-cluster can be found at cluster.ipfs.io

You need to specify an ipfs gateway and a screenshot directory, either
with --arguments or in the config file.

By default screenshot-ipfs uses scrot to capture screenshots, however
you can use other software with --screenshot-software.
EOM


# use_ipfs_cluster is no by default
use_ipfs_cluster=no

# use_mfs is yes by default
use_mfs=yes


# read config file if it exists
if [[ -e ~/.config/screenshot-ipfs.conf ]]; then
	source ~/.config/screenshot-ipfs.conf
fi

#parse arguments in "--option value" and "--option=value" syntax.
while [[ $# -gt 0 ]]; do
	i="$1"
	
	case $i in
		-g|--ipfs-gateway)
		ipfs_gateway="$2"
		shift # past argument
		shift # past value
		;;
		-d|--screenshot-dir)
		screenshot_dir="$2"
		shift # past argument
		shift # past value
		;;
		-c|--use-ipfs-cluster)
		use_ipfs_cluster="yes"
		shift # past argument
		;;
		-h|--usage)
		print_help=yes
		shift # past argument
		;;
		-m|--mfs-dir)
		mfs_dir="$2"
		shift #past argument
		;;
		-g=*|--ipfs-gateway=*)
		ipfs_gateway="${i#*=}"
		shift # past argument=value
		;;
		-d=*|--screenshot-dir=*)
		screenshot_dir="${i#*=}"
		shift # past argument=value
		;;
		-m=*|--mfs-dir=*)
		mfs_dir="${i#*=}"
		shift # past argument=value
		;;
		*)    # unknown option
		echo "Unknown option:" "$1"
		shift # past argument
		;;
	esac
done


# the actual meat of the script.
if [[ $print_help == "yes" ]]; then
	echo "$use"
elif [[ -z $screenshot_dir || -z $ipfs_gateway ]]; then
	
	# Throw errors if variables aren't set.
	if [[ -z $screenshot_dir ]]; then
		echo "Error: No screenshot directory specified!"
	fi
	if [[ -z $ipfs_gateway ]]; then
		echo "Error: No ipfs gateway specified!"
	fi
	
else
	
	# Print variables
	echo "ipfs gateway:" "$ipfs_gateway"
	echo "screenshot dir:" "$screenshot_dir"
	echo "use ipfs cluster?" "$use_ipfs_cluster"
	echo "mfs dir:" "$mfs_dir"
	
	
	# Take screenshot
	echo "taking screenshot..."
	sleep 0.1; scrot -s $screenshot_dir/%Y-%m-%d_%H-%M-%S.png
	
	# Extract the screenshot filename from its path.
	echo "getting filename from screenshot path..."
	screenshot_filename="$(ls -tr $screenshot_dir | tail -1)"
	echo "filename:" "$screenshot_filename"
	
	# get the screenshot path
	screenshot_path="$(echo "$screenshot_dir/""$screenshot_filename")"
	
	# Add screenshot to local ipfs node and save the hash.
	echo "adding screenshot to local node..."
	ipfs_hash="$(ipfs add -Qw --nocopy $screenshot_path)"
	
	# Organize screenshots in a /screenshots mfs directory
	if [[ $mfs_dir ]]; then
		echo "adding screenshot to /screenshots mfs directory..."
		ipfs files cp /ipfs/$ipfs_hash $mfs_dir/
	fi
	
	# Copy public gateway URL to the clipboard, for easy sharing in chats.
	echo "copying URL to clipboard..."
	echo "https://$ipfs_gateway/ipfs/$ipfs_hash/$screenshot_filename" | xclip -sel clip -i
	
	
	# Pin the screenshot to the cluster, don't output the status.
	if [[ $use_ipfs_cluster == yes ]]; then
			echo "pinning screenshot to cluster..."
			ipfs-cluster-ctl pin add --ns $ipfs_hash
	fi
	
fi

