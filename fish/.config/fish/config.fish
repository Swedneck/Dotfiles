source ~/.config/fish/colors.fish
source ~/.config/fish/functions/pkg_functions.fish

set -U EDITOR vim
set fish_greeting
set PATH $PATH /home/"$USER"/.bin

fish_vi_key_bindings

abbr -a -- - 'cd -'
abbr -a ... 'cd ../..'
abbr -a .... 'cd ../../..'
abbr -a ..... 'cd ../../../..'

if test -e ~/.config/fish/local.fish
	source ~/.config/fish/local.fish
end

if status --is-interactive
	function fish_title; printf "%s@%s" "$USER" (prompt_hostname); end; funcsave fish_title;
	neofetch
end
