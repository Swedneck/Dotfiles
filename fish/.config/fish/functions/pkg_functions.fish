# Get distro name and what distro it's based on
if test -f /etc/os-release
	set dotfiles_os (grep -Po '(?<!VARIANT_ID=)(?<=ID=)[a-z]\w+' /etc/os-release)
	set dotfiles_os_like (grep -Po '(?<=ID_LIKE=)([a-z])\w+' /etc/os-release)
end

if test "$dotfiles_os_like" = "arch"; or test "$dotfiles_os" = "manjaro"
	function inst
		trizen -S --needed --noedit $argv
	end
	function search 
		trizen -Ss $argv
	end
	function rem 
		trizen -R $argv
	end
	function upg 
		trizen -Syu $argv
	end
end

if test "$dotfiles_os_like" = "ubuntu"; or test "$dotfiles_os" = "ubuntu"; or test "$dotfiles_os" = "mint"
	function inst 
		sudo apt install $argv
	end
	function rem 
		sudo apt remove $argv
	end
	function upd 
		sudo apt update $argv
	end
	function upg 
		sudo apt upgrade -y $argv
	end
	function search
		apt search $argv
	end
end

if test "$dotfiles_os" = "fedora"
	function inst 
		sudo dnf install $argv
	end
	function search 
		dnf search $argv
	end
	function rem 
		sudo dnf remove $argv
	end
	function upg 
		sudo dnf upgrade $argv
	end
end

if test "$dotfiles_os" = "opensuse"
	function inst 
		sudo zypper in $argv
	end
	function search 
		zypper search $argv
	end
	function rem 
		sudo zypper remove $argv
	end
	function upg 
		sudo zypper up $argv
	end
end
