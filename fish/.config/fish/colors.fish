set fish_color_normal normal
set fish_color_command 73ba25 brgreen
set fish_color_quote 21a4df brblue
set fish_color_redirection 73ba25 brgreen
set fish_color_end white
set fish_color_error c02525 red
set fish_color_param 00a489 brcyan
#set fish_color_comment 
set fish_color_match 35b9ab brcyan
set fish_color_search_match 
set fish_color_operator 38ade2 brblue
set fish_color_escape 173f4f blue
set fish_color_autosuggestion b9dc92 brgreen
set fish_color_cancel 

# prompt colors
set fish_color_user 73ba25 brgreen
set fish_color_host 73ba25 brgreen
set fish_color_prompt_suffix 73ba25 brgreen
set fish_color_prompt_vcs 1aad95 cyan
set fish_color_prompt_at 35b9ab brcyan
set fish_color_cwd 38ade2 brblue
set fish_color_prompt_ttypts 35b9ab brcyan --background 173f4f cyan
set fish_color_prompt_ttypts_separator 35b9ab brcyan --background normal

# completion pager
set fish_pager_color_prefix 35b9ab brcyan
set fish_pager_color_completion 73ba25 brgreen
set fish_pager_color_description 
set fish_pager_color_progress 
set fish_pager_color_secondary 173f4f blue

# colors in less
set -e LESS_TERMCAP_md
set -e LESS_TERMCAP_me
set -e LESS_TERMCAP_se
set -e LESS_TERMCAP_so
set -e LESS_TERMCAP_ue
set -e LESS_TERMCAP_us

set -xU LESS_TERMCAP_md (printf "\e[01;31m")
set -xU LESS_TERMCAP_me (printf "\e[0m")
set -xU LESS_TERMCAP_se (printf "\e[0m")
set -xU LESS_TERMCAP_so (printf "\e[01;44;33m")
set -xU LESS_TERMCAP_ue (printf "\e[0m")
set -xU LESS_TERMCAP_us (printf "\e[01;32m")
