#!/usr/bin/zsh
autoload -U colors && colors;

if [[ tty == /dev/tty/. ]]
then
	if [[ $USER == "root" ]]
	then
		PROMPT="$FG[014]$BG[002]%y%{$reset_color%}$FG[015]|$FG[192]%n$FG[006]@$FG[003]%m$FG[015]:$FG[014]%~ $FG[011]%#%{$reset_color%} "
	else
		PROMPT="$FG[014]$BG[002]%y%{$reset_color%}$FG[015]|$FG[010]%n$FG[006]@$FG[003]%m$FG[015]:$FG[014]%~ $FG[011]%#%{$reset_color%} "
	fi
else
	if [[ $USER == "root" ]]
	then
		PROMPT="$FG[051]$BG[022]%y%{$reset_color%}$FG[231]|$FG[196]%n$FG[039]@$FG[214]%m$FG[231]:$FG[051]%~ $FG[226]%#%{$reset_color%} "
	else
		PROMPT="$FG[051]$BG[022]%y%{$reset_color%}$FG[231]|$FG[046]%n$FG[039]@$FG[214]%m$FG[231]:$FG[051]%~ $FG[226]%#%{$reset_color%} "
	fi
fi
